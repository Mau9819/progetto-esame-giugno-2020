package it.uniroma3.siw.esameGiugno.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.esameGiugno.model.Tag;
import it.uniroma3.siw.esameGiugno.model.Task;
import it.uniroma3.siw.esameGiugno.service.TagService;

@Component
public class TagValidator implements Validator {
	@Autowired
	TagService tagService;
	final Integer MAX_NAME_LENGTH = 100;
	final Integer MIN_NAME_LENGTH = 2;
	final Integer MAX_DESCRIPTION_LENGTH = 1000;
	final String[] COLOR = {"GREEN" , "VERDE" , "BLUE" , "BLU" , "RED" , "ROSSO", "YELLOW", "GIALLO"};

	@Override
	public boolean supports(Class<?> clazz) {
		return Task.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Tag tag = (Tag) target;
		String name = tag.getName().trim();
		String color = tag.getColor().trim();
		boolean trovato = false;

		if(name.trim().isEmpty())
			errors.rejectValue("name", "required");
		else if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH)
			errors.rejectValue("name", "size");
		else if (this.tagService.getTag(name)!= null)
			errors.rejectValue("name", "duplicate");
	
	for(String c : COLOR) {
		if(c.equals(color))
			trovato = true;
	}
	if(!trovato)
	errors.rejectValue("color", "required");
	
}
}