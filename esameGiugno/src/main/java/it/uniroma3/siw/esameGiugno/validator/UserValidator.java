package it.uniroma3.siw.esameGiugno.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.service.UserService;

@Component
public class UserValidator implements Validator {
	
	private final Integer MAX_NAME_LENGTH = 100;
	private final Integer MIN_NAME_LENGTH = 2;

	@Autowired
	UserService userService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
		
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;
		String firstName = user.getFirstName().trim();
		String lastName = user.getLastName().trim();
		
		if(firstName.trim().isEmpty())
			errors.rejectValue("firstName", "required");
		else if (firstName.length() < MIN_NAME_LENGTH || firstName.length() > MAX_NAME_LENGTH)
			errors.rejectValue("firstName", "size");
		
		if(lastName.trim().isEmpty())
			errors.rejectValue("lastName", "required");
		else if (lastName.length() < MIN_NAME_LENGTH || lastName.length() > MAX_NAME_LENGTH)
			errors.rejectValue("lastName", "size");
	}

}
