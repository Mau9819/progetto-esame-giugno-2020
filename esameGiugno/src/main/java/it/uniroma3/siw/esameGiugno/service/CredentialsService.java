package it.uniroma3.siw.esameGiugno.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.esameGiugno.model.Credentials;
import it.uniroma3.siw.esameGiugno.repository.CredentialsRepository;

@Service
public class CredentialsService {

	@Autowired
	protected CredentialsRepository credentialsRepository;
	
	@Autowired
    protected PasswordEncoder passwordEncoder;
	
	private String password;
	
	@Transactional
	public Credentials getCredentials(Long id) {
		Optional<Credentials> result = this.credentialsRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public Credentials getCredentials(String username) {
		Optional<Credentials> result = this.credentialsRepository.findByUsername(username);
		return result.orElse(null);
	}
	
	@Transactional
	public Credentials saveCredentials(Credentials credentials) {
		credentials.setPassword(this.passwordEncoder.encode(credentials.getPassword()));
		password = credentials.getPassword();
		credentials.setRole(Credentials.DEFAULT_ROLE);
	
		return this.credentialsRepository.save(credentials);
	}
	
	@Transactional
	public Credentials saveCredentialsNoChangePass(Credentials credentials) {
		credentials.setPassword(password);
		return this.credentialsRepository.save(credentials);
	}
	
	@Transactional
	public Credentials saveCredentialsAdmin(Credentials credentials) {
		
		credentials.setPassword(password);
		credentials.setRole(Credentials.ADMIN_ROLE);
		
		return this.credentialsRepository.save(credentials);
	}
	
	@Transactional
    public List<Credentials> getAllCredentials() {
        List<Credentials> result = new ArrayList<>();
        Iterable<Credentials> iterable = this.credentialsRepository.findAll();
        for(Credentials credentials : iterable)
            result.add(credentials);
        return result;
	}
	
	@Transactional
	public void deleteCredentials(Credentials credentials) {
	    this.credentialsRepository.deleteById(credentials.getId());
	}

	@Transactional
	public void deleteCredentials(String username) {
	    this.credentialsRepository.deleteByUsername(username);
	}

}