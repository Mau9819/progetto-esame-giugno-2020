package it.uniroma3.siw.esameGiugno.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.esameGiugno.model.Project;
import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Transactional
	public User getUser(Long id) {
		Optional<User> result = this.userRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public User saveUser(User user) {
		return this.userRepository.save(user);
	}
	
	@Transactional
	public List<User> getAllUsers(){
		Iterable<User> u = this.userRepository.findAll();
		List<User> l = new ArrayList<>();
		for(User us : u) {
			l.add(us);
		}
		return l;
	}
	
	@Transactional
	public List<User> getMembers(Project project){
		Iterable<User> u =this.userRepository.findByVisibleProject(project);
		List<User> l = new ArrayList<>();
		for(User us : u) {
			l.add(us);
		}
		return l;
	}
}
