package it.uniroma3.siw.esameGiugno.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.esameGiugno.model.Tag;
import it.uniroma3.siw.esameGiugno.repository.TagRepository;

@Service
public class TagService {

	
	@Autowired
	private TagRepository tagRepository;
	
	@Transactional
	public Tag getTag(Long id) {
		Optional<Tag> result = this.tagRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional 
	public Tag getTag(String name) {
		Optional<Tag> result = this.tagRepository.findByName(name);
		return result.orElse(null);
	}
	
	@Transactional
	public void deleteTag(Long id) {
		this.tagRepository.deleteById(id);
	}
	
	
}
