package it.uniroma3.siw.esameGiugno.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.esameGiugno.model.Project;
import it.uniroma3.siw.esameGiugno.model.User;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {
	
	public List<User> findByVisibleProject(Project project);
}
