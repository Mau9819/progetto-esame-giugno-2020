package it.uniroma3.siw.esameGiugno.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.esameGiugno.model.Credentials;
import it.uniroma3.siw.esameGiugno.model.Project;
import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.repository.CredentialsRepository;
import it.uniroma3.siw.esameGiugno.repository.UserRepository;
import it.uniroma3.siw.esameGiugno.service.CredentialsService;
import it.uniroma3.siw.esameGiugno.service.ProjectService;
import it.uniroma3.siw.esameGiugno.service.TaskService;
import it.uniroma3.siw.esameGiugno.service.UserService;
import it.uniroma3.siw.esameGiugno.session.SessionData;
import it.uniroma3.siw.esameGiugno.validator.CredentialsValidator;
import it.uniroma3.siw.esameGiugno.validator.UserValidator;

	@Controller
	public class UserController {

	    @Autowired
	    UserRepository userRepository;

	    @Autowired
	    UserValidator userValidator;
	    
	    @Autowired
	    UserService userService;

	    @Autowired
	    CredentialsValidator credentialsValidator;

	    @Autowired
	    PasswordEncoder passwordEncoder;

	    @Autowired
	    SessionData sessionData;
	    
	    @Autowired
	    CredentialsService credentialsService;

	    @Autowired
	    CredentialsRepository credentialsRepository;
	    
	    @Autowired
	    ProjectService projectService;
	    
	    @Autowired
	    TaskService taskService;


	    /**
	     * This method is called when a GET request is sent by the user to URL "/users/user_id".
	     * This method prepares and dispatches the User registration view.
	     *
	     * @param model the Request model
	     * @return the name of the target view, that in this case is "register"
	     */
	    @RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	    public String home(Model model) {
	        User loggedUser = sessionData.getLoggedUser();
	        model.addAttribute("user", loggedUser);
	        return "home";
	    }

	    /**
	     * This method is called when a GET request is sent by the user to URL "/users/user_id".
	     * This method prepares and dispatches the User registration view.
	     *
	     * @param model the Request model
	     * @return the name of the target view, that in this case is "register"
	     */
	    @RequestMapping(value = { "/users/me" }, method = RequestMethod.GET)
	    public String me(Model model) {
	        User loggedUser = sessionData.getLoggedUser();
	        Credentials credentials = sessionData.getLoggedCredentials();
	      //  System.out.println(credentials.getPassword());
	        model.addAttribute("user", loggedUser);
	        model.addAttribute("credentials", credentials);

	        return "userProfile";
	    }

	    /**
	     * This method is called when a GET request is sent by the user to URL "/users/user_id".
	     * This method prepares and dispatches the User registration view.
	     *
	     * @param model the Request model
	     * @return the name of the target view, that in this case is "register"
	     */
	    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
	    public String admin(Model model) {
	        User loggedUser = sessionData.getLoggedUser();
	        model.addAttribute("user", loggedUser);
	        return "admin";
	    }
	    
	    @RequestMapping(value = { "/add/admin" }, method = RequestMethod.GET)
	    public String adminAdd(Model model) {
	        User loggedUser = sessionData.getLoggedUser();
	        model.addAttribute("user", loggedUser);
	        return "newAdmin";
	    }
	    
	    
		@RequestMapping(value = { "/add/admin" }, method = RequestMethod.POST)
	    public String newadminAdd(Model model) {
	        User loggedUser = sessionData.getLoggedUser();
	        Credentials credentials = sessionData.getLoggedCredentials();
	        
	        credentials.setRole(Credentials.ADMIN_ROLE);
	        
	        credentialsService.saveCredentialsAdmin(credentials);
	        
	        model.addAttribute("user", loggedUser);
	        model.addAttribute("credentials", credentials);
	        return "index";
	    }
	    
	    @RequestMapping(value = {"/admin/users"}, method = RequestMethod.GET)
	    public String userList(Model model) {
	    	User loggedUser = sessionData.getLoggedUser();
	    	List<Credentials> allCredentials = this.credentialsService.getAllCredentials();
	    	model.addAttribute("loggedUser", loggedUser);
	    	model.addAttribute("credentialsList", allCredentials);
	    	
	    	return "allUsers";
	    }

	    @RequestMapping(value = {"/admin/users/{username}/delete"}, method = RequestMethod.POST)
	    public String removeUser(Model model, @PathVariable String username) {
			Credentials credentials  = this.credentialsService.getCredentials(username);
			User userDelete = credentials.getUser();
            if(!userDelete.getOwnedProject().isEmpty())
            	for(Project p : userDelete.getOwnedProject()) {
            		List<User> userProject = p.getMembers();
            		for(User us : userProject) {
            			us.getVisibleProject().remove(p);
            			
            		}
            	}
            	 
         	
	    	this.credentialsService.deleteCredentials(username);
	    	return "userDeleted";
	    	
	    }
	    
	    @RequestMapping(value = {"/change/username"}, method = RequestMethod.GET)
		public String changeUsernameForm(Model model) {
			User loggedUser = sessionData.getLoggedUser();
		    Credentials credentials = sessionData.getLoggedCredentials(); 
		    model.addAttribute("credentials", credentials);
			model.addAttribute("user", loggedUser);
			model.addAttribute("changeUsernameForm", new Credentials());			
			return "changeUsername";
		}	

	    @RequestMapping(value = {"/change/username"}, method = RequestMethod.POST)
		public String changeUsername(@Validated @ModelAttribute("changeUsernameForm") Credentials credentials, BindingResult credentialsBidingResult,Model model) {
	    	User loggedUser = sessionData.getLoggedUser();
	        Credentials oldCredential = sessionData.getLoggedCredentials();
	        credentials.setPassword(oldCredential.getPassword());
	        credentialsValidator.validate(credentials, credentialsBidingResult);
	        if(!credentialsBidingResult.hasErrors()) {	
	        	    oldCredential.setUsername(credentials.getUsername());
	        		credentialsService.saveCredentialsNoChangePass(oldCredential);
	        	    model.addAttribute("user", loggedUser);
	     	        model.addAttribute("credentials", oldCredential);
	        		return "userProfile";
	        }
	        
	        model.addAttribute("user", loggedUser);
	        model.addAttribute("credentials", oldCredential);
	        return "changeUsername";
	    
	    }
	    
	    
	    @RequestMapping(value = {"/change/nameLast"}, method = RequestMethod.GET)
		public String changeNameLastForm(Model model) {
			User loggedUser = sessionData.getLoggedUser();
		    Credentials credentials = sessionData.getLoggedCredentials(); 
		    User userNuovo = loggedUser;
		    model.addAttribute("credentials", credentials);
			model.addAttribute("user", loggedUser);
			model.addAttribute("changeNameLastForm", userNuovo);			
			return "changeNameLast";
		}	

	    @RequestMapping(value = {"/change/nameLast"}, method = RequestMethod.POST)
		public String changeNameorLast(@Validated @ModelAttribute("changeNameLastForm") User userNuovo, BindingResult userBidingResult,Model model) {
	    	User oldloggedUser = sessionData.getLoggedUser();
	        Credentials Credential = sessionData.getLoggedCredentials();	
	   
	        userValidator.validate(userNuovo, userBidingResult);
	        if(!userBidingResult.hasErrors()) {	
	        	oldloggedUser.setFirstName(userNuovo.getFirstName());
	    	    oldloggedUser.setLastName(userNuovo.getLastName());
	    	    userService.saveUser(oldloggedUser);
	    	    model.addAttribute("user", oldloggedUser);
	    	    model.addAttribute("credentials", Credential);
	    	    
	    	    return "userProfile";
	        	}
	    
	        model.addAttribute("user", oldloggedUser);
            model.addAttribute("credentials", Credential);
            return "changeNameLast";    
	      }    
}