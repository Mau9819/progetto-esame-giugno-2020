package it.uniroma3.siw.esameGiugno.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.esameGiugno.model.Project;
import it.uniroma3.siw.esameGiugno.model.Task;
import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.service.ProjectService;
import it.uniroma3.siw.esameGiugno.service.TaskService;
import it.uniroma3.siw.esameGiugno.service.UserService;
import it.uniroma3.siw.esameGiugno.session.SessionData;
import it.uniroma3.siw.esameGiugno.validator.TaskValidator;

@Controller
public class TaskController {

	@Autowired
	TaskService taskService;

	@Autowired
	ProjectService projectService;

	@Autowired 
	SessionData sessionData;

	@Autowired
	TaskValidator taskValidator;

	@Autowired
	UserService userService;

	Long idAttuale;

	@RequestMapping(value = {"/addTask/{projectId}"}, method = RequestMethod.GET)
	public String createTaskForm(Model model, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		if(project == null) 
			return "redirect:/projects";
		List<User> members = userService.getMembers(project);
		if(!project.getOwner().equals(sessionData.getLoggedUser()) && !members.contains(sessionData.getLoggedUser()))
			return "redirect:/projects";
		//model.addAttribute("user", sessionData.getLoggedUser());
		model.addAttribute("project", project);
		model.addAttribute("taskForm", new Task());
		this.idAttuale = projectId;
		return "addTask";
	}	

	@RequestMapping(value = {"/addTask"}, method = RequestMethod.POST)
	public String createTask(@Validated @ModelAttribute("taskForm") Task task, BindingResult taskBindingResult,Model model) {

		Project project = this.projectService.getProject(idAttuale);

		taskValidator.validate(task, taskBindingResult);
		if(!taskBindingResult.hasErrors()) 
			if(idAttuale !=null) {
				project.addTask(task);
				this.projectService.saveProject(project);
				return "redirect:/projects/" + idAttuale;

			}
		model.addAttribute("project", project);
		return "addTask";	
	}	

	@RequestMapping(value = {"/projects/task/{id}/{idProject}/delete"}, method = RequestMethod.POST)
	public String deleteTask(Model model, @PathVariable Long id , @PathVariable Long idProject ) {
		//idAttuale non va bene come parametro

		Project project = this.projectService.getProject(idProject);
		Task task = this.taskService.getTask(id);
		project.getTasks().remove(task);
		this.projectService.saveProject(project);
		this.taskService.deleteTask(id);
		return "Deleted";
	}

	@RequestMapping(value = {"/projects/task/{id}/{idProject}/share"}, method = RequestMethod.GET)
	public String shareTask(Model model, @PathVariable Long id , @PathVariable Long idProject ) {
		User loggedUser = this.sessionData.getLoggedUser();
		Project project = this.projectService.getProject(idProject);
		List<User> members = this.userService.getMembers(project);
		members.remove(loggedUser);
		if(members.isEmpty()) {
			return "NotMember";
		}
		Task task = this.taskService.getTask(id);
		model.addAttribute("project", project);
		model.addAttribute("members", members);
		model.addAttribute("task", task);
		return "ShareTask";
	}

	@RequestMapping(value = {"/projects/task/{id}/{idMember}/sharing"}, method = RequestMethod.POST)
	public String sharingTask(Model model, @PathVariable Long id , @PathVariable Long idMember) {
		User member = this.userService.getUser(idMember);
		Task task = this.taskService.getTask(id);
		if(task.getUserAssigned() != null && task.getUserAssigned().equals(member)) {
			return "alreadyShared";
		}
		task.setUserAssigned(member);
		this.taskService.saveTask(task);
		return "Shared";
	}

	@RequestMapping(value = {"/task/assigned/{idProject}"}, method = RequestMethod.GET)
	public String myTask(Model model, @PathVariable Long idProject) {
		User loggedUser = this.sessionData.getLoggedUser();
		Project project = this.projectService.getProject(idProject);
		if(verifyTaskForProjectUser(project, loggedUser)) {
			return "NoTasks";
		} 
		List<Task> myTasks = this.getTaskForProjectUser(project, loggedUser);
		model.addAttribute("project",project);
		model.addAttribute("myTasks", myTasks);
		return "MyTasks";
	}


	private List<Task> getTaskForProjectUser(Project project, User loggedUser) {
		List<Task> allTasks = project.getTasks();
		List<Task> assignedTask = new ArrayList<>();
		for(Task t : allTasks) {
			if(t != null && t.getUserAssigned().equals(loggedUser)) {
				assignedTask.add(t);
			}
		}
		return assignedTask;
	}

	private boolean verifyTaskForProjectUser(Project project, User loggedUser) {
		List<Task> allTasks = project.getTasks();
		if(allTasks.isEmpty())
			return true;
		List<Task> assignedTask = new ArrayList<>();
		for(Task t : allTasks) {
			if(t.getUserAssigned()!= null && t.getUserAssigned().equals(loggedUser)) {
				assignedTask.add(t);
			}
		}
		if(assignedTask.isEmpty())
			return true;
		return false;
	}
	

	@RequestMapping(value = {"/projects/task/{id}/ChangeNameDesc"}, method = RequestMethod.GET)
	public String changeNameDescForm(Model model, @PathVariable Long id  ) {
		Task task = this.taskService.getTask(id);
		Task tasknuovo = task;
		model.addAttribute("changeNameDescForm", tasknuovo);
		model.addAttribute("task", task);
		return "changeNameOrDescTask";
	}
	
	@RequestMapping(value = {"/projects/task/{id}/ChangeNameDesc"}, method = RequestMethod.POST)
	public String changeNameDesc(@Validated @ModelAttribute("changeNameDescForm") Task tasknuovo, BindingResult taskBidingResult, Model model, @PathVariable Long id  ) {
		Task taskOriginal = this.taskService.getTask(id);
		User loggedUser = sessionData.getLoggedUser();
		taskValidator.validate(tasknuovo, taskBidingResult);
		 if(!taskBidingResult.hasErrors()) {
			 taskOriginal.setName(tasknuovo.getName());
			 taskOriginal.setDescription(tasknuovo.getDescription());
			 taskService.saveTask(taskOriginal);
			 List<Project> projectList = projectService.visualizzaProgetti(loggedUser);
				model.addAttribute("loggedUser", loggedUser);
				model.addAttribute("projectsList", projectList);
				return "projects";
		 }
		 if(tasknuovo.getName().equals(taskOriginal.getName())) {
			 taskOriginal.setDescription(tasknuovo.getDescription());
			 taskService.saveTask(taskOriginal);
			 List<Project> projectList = projectService.visualizzaProgetti(loggedUser);
				model.addAttribute("loggedUser", loggedUser);
				model.addAttribute("projectsList", projectList);
				return "projects";
		 }
		 model.addAttribute("task", taskOriginal);
		return "changeNameOrDescTask";
	}
}











