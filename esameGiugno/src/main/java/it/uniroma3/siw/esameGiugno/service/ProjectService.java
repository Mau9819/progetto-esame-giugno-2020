package it.uniroma3.siw.esameGiugno.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.esameGiugno.model.Project;
import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.repository.ProjectRepository;

@Service
public class ProjectService {

	@Autowired
	private ProjectRepository projectRepository;
	
	
	@Transactional
	public List<Project> visualizzaProgetti(User user) {
		Iterable<Project> p = this.projectRepository.findByOwner(user);
		List<Project> l = new ArrayList<>();
		for(Project pr : p) {
			l.add(pr);
		}
		return l;
	}
	
	@Transactional
	public Project getProject(Long id) {
		Optional<Project> result = this.projectRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public Project getProject(String nome) {
		Optional<Project> result = this.projectRepository.findByName(nome);
		return result.orElse(null);
	}
	
	@Transactional
	public Project saveProject(Project project) {
		return this.projectRepository.save(project);
	}
	
	@Transactional
	public void deleteProject(Long id) {
		this.projectRepository.deleteById(id);
	}
	

	@Transactional
	public void deleteOwnedProject(User user) {
        List<Project> ownedProject = user.getOwnedProject();
        for(Project p : ownedProject)
            this.projectRepository.deleteById(p.getId());     
	}
	
	@Transactional
	public Project shareProjectWithUser(Project project, User user) {
		project.addmember(user);
		return this.projectRepository.save(project);
	}
	
}
