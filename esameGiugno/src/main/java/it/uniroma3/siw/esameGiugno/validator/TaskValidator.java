package it.uniroma3.siw.esameGiugno.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.esameGiugno.model.Task;
import it.uniroma3.siw.esameGiugno.service.TaskService;

@Component
public class TaskValidator implements Validator {

	@Autowired
	TaskService taskService;
	final Integer MAX_NAME_LENGTH = 100;
	final Integer MIN_NAME_LENGTH = 2;
	final Integer MAX_DESCRIPTION_LENGTH = 1000;

	@Override
	public boolean supports(Class<?> clazz) {
		return Task.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Task task = (Task) target;
		String name = task.getName().trim();
		String description = task.getDescription().trim();

		if(name.trim().isEmpty())
			errors.rejectValue("name", "required");
		else if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH)
			errors.rejectValue("name", "size");
		else if(description.trim().isEmpty())
			errors.rejectValue("description", "required");
		else if(description.length() > MAX_DESCRIPTION_LENGTH)
			errors.rejectValue("description", "size");
		else if (this.taskService.getTask(name)!= null)
			errors.rejectValue("name", "duplicate");

	}
}
