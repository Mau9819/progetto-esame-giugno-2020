package it.uniroma3.siw.esameGiugno.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.esameGiugno.model.Credentials;
import it.uniroma3.siw.esameGiugno.model.Project;
import it.uniroma3.siw.esameGiugno.model.Tag;
import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.repository.ProjectRepository;
import it.uniroma3.siw.esameGiugno.service.CredentialsService;
import it.uniroma3.siw.esameGiugno.service.ProjectService;
import it.uniroma3.siw.esameGiugno.service.UserService;
import it.uniroma3.siw.esameGiugno.session.SessionData;
import it.uniroma3.siw.esameGiugno.validator.ProjectValidator;
import it.uniroma3.siw.esameGiugno.validator.TagValidator;

@Controller
public class ProjectController {

	@Autowired
	ProjectService projectService;

	@Autowired
	UserService userService;

	@Autowired
	ProjectValidator projectValidator;
	
	@Autowired
	TagValidator tagValidator;

	@Autowired
	SessionData sessionData;

	@Autowired
	CredentialsService credentialsService;

	@Autowired
	ProjectRepository projectRepository;


	private Long idAttuale;

	@RequestMapping(value = {"/projects"}, method = RequestMethod.GET)
	public String myOwnedProjects(Model model) {
		User loggedUser = sessionData.getLoggedUser();
		List<Project> projectList = projectService.visualizzaProgetti(loggedUser);
		if(projectList == null || projectList.isEmpty()) {
			return "NoProject";
		}
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("projectsList", projectList);
		return "projects";
	}

	@RequestMapping(value = {"/projects/{projectId}"}, method = RequestMethod.GET)
	public String project(Model model, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		if(project == null) 
			return "redirect:/projects";

		List<User> members = userService.getMembers(project);
		if(!project.getOwner().equals(sessionData.getLoggedUser()) && !members.contains(sessionData.getLoggedUser()))
			return "redirect:/projects";
		model.addAttribute("user", sessionData.getLoggedUser());
		model.addAttribute("project", project);
		model.addAttribute("members", members);
		return "projectViewer";
	}

	@RequestMapping(value = {"/projects/tag/{projectId}"}, method = RequestMethod.GET)
	public String tagProject(Model model, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		if(project == null) 
			return "redirect:/projects";
		List<Tag> tagsList = project.getTags();
		model.addAttribute("project", project);
		model.addAttribute("tags", tagsList);
		return "tagViewer";
	}
	
	@RequestMapping(value = {"/project/add"}, method = RequestMethod.GET)
	public String createProjectForm(Model model) {
		User loggedUser = sessionData.getLoggedUser();

		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("projectForm", new Project());
		return "projectAdd";
	}	

	@RequestMapping(value = {"/project/add"}, method = RequestMethod.POST)
	public String createProject(@Validated @ModelAttribute("projectForm") Project project, BindingResult projectBindingResult,Model model) {
		User loggedUser = sessionData.getLoggedUser();
		projectValidator.validate(project, projectBindingResult);
		if(!projectBindingResult.hasErrors()) {
			project.setOwner(loggedUser);
			this.projectService.saveProject(project);
			return "redirect:/projects/" + project.getId();
		}

		model.addAttribute("loggedUser", loggedUser);
		return "projectAdd";
	}


	@RequestMapping(value = {"/projects/project/{id}/delete"}, method = RequestMethod.POST)
	public String removeProject(Model model, @PathVariable Long id) {
		this.projectService.deleteProject(id);
		return "Deleted";
	}

	@RequestMapping(value = {"/projects/share/{id}/sharing"}, method = RequestMethod.GET)
	public String shareProjectGet(Model model, @PathVariable Long id) {
		idAttuale = id;
		Project project = this.projectService.getProject(id);
		Credentials loggedCredentials = sessionData.getLoggedCredentials();
		List<Credentials> allCredentials = this.credentialsService.getAllCredentials();
		allCredentials.remove(loggedCredentials);
		allCredentials = removeMembers(allCredentials, project);
		model.addAttribute("allCredentials", allCredentials);
		model.addAttribute("project",project);
		return "allUsersForShare";
	}

	//funzione che elimina dalla lista coloro che già sono membri
	private List<Credentials> removeMembers(List<Credentials> allCredentials, Project project) {
	List<Credentials> result = new ArrayList<Credentials>();
		for(Credentials c : allCredentials) {
			if(!c.getUser().getVisibleProject().contains(project)) {
				result.add(c);
			}
		}
		
		return result;
	}

	@RequestMapping(value = {"/projects/share/{id}/sharing/user"}, method = RequestMethod.POST)
	public String shareProjectPost(Model model, @PathVariable Long id) {
		User user = this.userService.getUser(id);
		Project project = this.projectService.getProject(idAttuale);
		this.projectService.shareProjectWithUser(project, user);
		return "shared";
	}

	@RequestMapping(value = { "/member"}, method = RequestMethod.GET)
	public String shareWithMe(Model model) {
		User loggedUser = this.sessionData.getLoggedUser();
		List<Project> sharedProjects = this.projectRepository.findByMembers(loggedUser);
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("sharedProjects", sharedProjects);
		return "sharedProjectWithMe";
	}
	
	@RequestMapping(value = {"/projects/project/{id}/ChangeNameDesc"}, method = RequestMethod.GET)
	public String changeNameDescForm(Model model, @PathVariable Long id  ) {
		Project project = this.projectService.getProject(id);
		Project projectnuovo = project;
		model.addAttribute("changeNameDescForm", projectnuovo);
		model.addAttribute("project", project);
		return "changeNameOrDescProject";
	}
	

	@RequestMapping(value = {"/projects/project/{id}/ChangeNameDesc"}, method = RequestMethod.POST)
	public String changeNameDesc(@Validated @ModelAttribute("changeNameDescForm") Project projectnuovo, BindingResult projectBidingResult, Model model, @PathVariable Long id  ) {
		Project projectOriginal = this.projectService.getProject(id);
		User loggedUser = sessionData.getLoggedUser();
		projectValidator.validate(projectnuovo, projectBidingResult);
		 if(!projectBidingResult.hasErrors()) {
			 projectOriginal.setName(projectnuovo.getName());
			 projectOriginal.setDescription(projectnuovo.getDescription());
			 projectService.saveProject(projectOriginal);
			 List<Project> projectList = projectService.visualizzaProgetti(loggedUser);
				model.addAttribute("loggedUser", loggedUser);
				model.addAttribute("projectsList", projectList);
				return "projects";
		 }
		 if(projectnuovo.getName().equals(projectOriginal.getName())) {
			 projectOriginal.setDescription(projectnuovo.getDescription());
			 projectService.saveProject(projectOriginal);
			 List<Project> projectList = projectService.visualizzaProgetti(loggedUser);
				model.addAttribute("loggedUser", loggedUser);
				model.addAttribute("projectsList", projectList);
				return "projects";
		 }
		 model.addAttribute("task", projectOriginal);
		return "changeNameOrDescTask";
	}

	@RequestMapping(value = {"/projects/project/{id}/addTag"}, method = RequestMethod.GET)
	public String addTagForm(Model model, @PathVariable Long id) {
			Project project = this.projectService.getProject(id);
			model.addAttribute("project", project);
			model.addAttribute("addTagForm", new Tag());
			return "addTagProject";
		}
	
	@RequestMapping(value = {"/projects/project/{id}/addTag"}, method = RequestMethod.POST)
	public String addTag(@Validated @ModelAttribute("addTagForm") Tag tagNuovo, BindingResult tagBidingResult,Model model, @PathVariable Long id) {
		User loggedUser = sessionData.getLoggedUser();
		Project projectnuovo = this.projectService.getProject(id);
		tagValidator.validate(tagNuovo, tagBidingResult);
		if(!tagBidingResult.hasErrors()) {
		projectnuovo.addTag(tagNuovo);
			projectService.saveProject(projectnuovo);
			model.addAttribute("user", loggedUser);
			return "home";
		}
		model.addAttribute("project", projectnuovo);
		return "addTagProject";
		
	}
	
//	<form method="POST"
//			th:action="@{/projects/{id}/{idProject}/unshare(id=${member.id} , idProject=${project.id} )}">
//			<button type="submit" name="submit" value="value">UNSHARED</button>
//		</form>
	@RequestMapping(value = {"/projects/{id}/{idProject}/unshare"}, method = RequestMethod.POST)
	public String unShareProject(Model model, @PathVariable Long id, @PathVariable Long idProject) {
		User member = this.userService.getUser(id);
		Project project = this.projectService.getProject(idProject);
		project.getMembers().remove(member);
		this.projectRepository.save(project);
		return "Unshared";
		
	}
	
}
