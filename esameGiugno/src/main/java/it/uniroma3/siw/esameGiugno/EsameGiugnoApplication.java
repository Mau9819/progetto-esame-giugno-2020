package it.uniroma3.siw.esameGiugno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsameGiugnoApplication {

	//occhio alla password su application.properties
	public static void main(String[] args) {
		SpringApplication.run(EsameGiugnoApplication.class, args);
	}

}
