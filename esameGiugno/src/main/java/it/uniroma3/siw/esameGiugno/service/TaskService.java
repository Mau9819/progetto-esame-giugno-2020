package it.uniroma3.siw.esameGiugno.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.esameGiugno.model.Task;
import it.uniroma3.siw.esameGiugno.repository.TaskRepository;

@Service
public class TaskService{
	
	@Autowired
	private TaskRepository taskRepository;

	@Transactional
	public Task getTask(Long id) {
		Optional<Task> result = this.taskRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public Task getTask(String name) {
		Optional<Task> result = this.taskRepository.findByName(name);
		return result.orElse(null);
	}
	
	@Transactional
	public Task saveTask(Task task) {
		return this.taskRepository.save(task);
	}
	
	@Transactional
	public void deleteTask(Long id) {
		this.taskRepository.deleteById(id);
	}
}