package it.uniroma3.siw.esameGiugno.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;


@Entity
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//Adesso se ne occupa la classe Credentials
//	@Column(unique = true, nullable = false, length = 100)
//	private String username;
//	
//	@Column(nullable = false, length = 100)
//	private String password;
	
	@Column(nullable = false, length = 100)
	private String firstName;
	
	@Column(nullable = false, length = 100)
	private String lastName;
	
	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "owner")
	private List<Project> ownedProject;
	
	@ManyToMany(cascade = CascadeType.REMOVE, mappedBy = "members")
	private List<Project> visibleProject;
	
	@Column(updatable = false, nullable = false)
	private LocalDateTime creationTimestamp;
	
	public User() { 
		this.ownedProject = new ArrayList<>();
		this.visibleProject = new ArrayList<>();
	}
	
	public User(String firstName, String lastName) {
		this();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@PrePersist
	protected void onPersist() {
		this.creationTimestamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//Adesso se ne occupa la classe Credentials
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}

	//Adesso se ne occupa la classe Credentials
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Project> getOwnedProject() {
		return ownedProject;
	}

	public void setOwnedProject(List<Project> ownedProject) {
		this.ownedProject = ownedProject;
	}

	public List<Project> getVisibleProject() {
		return visibleProject;
	}

	public void setVisibleProject(List<Project> visibleProject) {
		this.visibleProject = visibleProject;
	}
	
	//SECONDO ME NON DOVREBBERO STARE QUI
//	//creazione di un progetto
//	//se ritorna true allora è stato aggiunto con successo
//	public boolean createProject(String name, String description) {
//		Project project = new Project(name);
//		boolean result;
//		result = this.ownedProject.add(project);
//		return result;
//	}
//
//	//aggiungo task a un progetto
//	//se il progetto passato non appartiene allo user corrente ritorna false
//	public boolean addTask(Project project, Task task) {
//		if(this.ownedProject.contains(project)) {
//			int i = this.ownedProject.indexOf(project);
//			this.ownedProject.get(i).addTask(task);
//			return true;
//		}
//		else 
//			return false;
//	}
//
//	public void visibleProject(User user, Project project) {
//		user.getVisibleProject().add(project);
//	}
	public LocalDateTime getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(LocalDateTime creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationTimestamp == null) ? 0 : creationTimestamp.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (creationTimestamp == null) {
			if (other.creationTimestamp != null)
				return false;
		} else if (!creationTimestamp.equals(other.creationTimestamp))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
	
	
}
