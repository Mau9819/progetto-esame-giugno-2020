package it.uniroma3.siw.esameGiugno.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.esameGiugno.model.Credentials;

@Repository
public interface CredentialsRepository extends CrudRepository<Credentials, Long>{

	public Optional<Credentials> findByUsername(String username);
	
	public Optional<Credentials> deleteByUsername(String username);
	
	
}
