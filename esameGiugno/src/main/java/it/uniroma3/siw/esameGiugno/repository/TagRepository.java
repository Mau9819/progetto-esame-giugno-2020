package it.uniroma3.siw.esameGiugno.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.esameGiugno.model.Tag;

@Repository
public interface TagRepository extends CrudRepository<Tag,Long>{
	
	public Optional<Tag> findByName(String name);

}
