package it.uniroma3.siw.esameGiugno.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.esameGiugno.model.Credentials;
import it.uniroma3.siw.esameGiugno.model.User;
import it.uniroma3.siw.esameGiugno.service.CredentialsService;

@Component
public class CredentialsValidator implements Validator{

	@Autowired
	CredentialsService credentialsService;
	private final Integer MAX_USERNAME_LENGTH = 20;
	private final Integer MIN_USERNAME_LENGTH = 4;
	private final Integer MAX_PASSWORD_LENGTH = 20;
	private final Integer MIN_PASSWORD_LENGTH = 6;

	
	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);

	}

	@Override
	public void validate(Object target, Errors errors) {
		Credentials credentials = (Credentials) target;
		String username = credentials.getUsername().trim();
		String password = credentials.getPassword().trim();

		if(username.trim().isEmpty())
			errors.rejectValue("username", "required");
		else if (username.length() < MIN_USERNAME_LENGTH || username.length() > MAX_USERNAME_LENGTH)
			errors.rejectValue ("username", "size");
		else if (this.credentialsService.getCredentials(username) != null)
			errors.rejectValue("username", "duplicate");

		if(password.trim().isEmpty())
			errors.rejectValue("password", "required");
		else if (password.length() < MIN_PASSWORD_LENGTH || password.length() > MAX_PASSWORD_LENGTH)
			errors.rejectValue("password", "size");

	}
}
